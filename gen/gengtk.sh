#!/bin/bash
cd `dirname "$0"`
set -e
if [ "$1" == "" ]; then
  echo Usage: $0 /path/to/pygtk
  exit 1
fi
PYGTK="$1"
drun gengtk.dlt "$PYGTK/gtk/gtk.defs" "$PYGTK/gtk/gdk.defs" "$PYGTK/pango.defs" > tmp.dlt
./doenums.py tmp.dlt ../dlt/gtk.dlt
