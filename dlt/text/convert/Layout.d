/*******************************************************************************

        copyright:      Copyright (c) 2005 Kris. All rights reserved

        license:        BSD style: $(LICENSE)

        version:        Initial release: 2005

        author:         Kris, Keinfarbton

        This module provides a general-purpose formatting system to
        convert values to text suitable for display. There is support
        for alignment, justification, and common format specifiers for
        numbers.

        Layout can be customized via configuring various handlers and
        associated meta-data. This is utilized to plug in text.locale
        for handling custom formats, date/time and culture-specific
        conversions.

        The format notation is influenced by that used by the .NET
        and ICU frameworks, rather than C-style printf or D-style
        writef notation.

******************************************************************************/

module dlt.text.convert.Layout;

private import  dlt.core: IllegalArgumentException;
private import  Utf = dlt.text.convert.Utf;

private import  Float  = dlt.text.convert.Float,
                Integer = dlt.text.convert.Integer;

alias        char [] Mutf8;
alias       wchar [] Mutf16;
alias       dchar [] Mutf32;

alias const( char)[] Cutf8;
alias const(wchar)[] Cutf16;
alias const(dchar)[] Cutf32;

/*******************************************************************************

        Platform issues ...

*******************************************************************************/

version (GNU)
        {
        private import std.stdarg;
        alias void* Arg;
        alias va_list ArgList;
        }
     else
        {
        alias void* Arg;
        alias void* ArgList;
        }

/*******************************************************************************

        Contains methods for replacing format items in a Cutf8 with Cutf8
        equivalents of each argument.

*******************************************************************************/

template Layout(T)
{
        public alias convert opCall;
        public alias uint delegate (const(T)[]) Sink;

        /**********************************************************************

        **********************************************************************/

        public final T[] sprint (T[] result, const(T)[] formatStr, ...)
        {
                return vprint (result, formatStr, _arguments, _argptr);
        }

        /**********************************************************************

        **********************************************************************/

        public final T[] vprint (T[] result, const(T)[] formatStr, TypeInfo[] arguments, ArgList args)
        {
                T*  p = result.ptr;
                int available = result.length;

                uint sink (const(T)[] s)
                {
                        int len = s.length;
                        if (len > available)
                            len = available;

                        available -= len;
                        p [0..len] = s[0..len];
                        p += len;
                        return len;
                }

                convert (&sink, arguments, args, formatStr);
                return result [0 .. p-result.ptr];
        }

        /**********************************************************************

                Replaces the _format item in a Cutf8 with the Cutf8
                equivalent of each argument.

                Params:
                  formatStr  = A Cutf8 containing _format items.
                  args       = A list of arguments.

                Returns: A copy of formatStr in which the items have been
                replaced by the Cutf8 equivalent of the arguments.

                Remarks: The formatStr parameter is embedded with _format
                items of the form: $(BR)$(BR)
                  {index[,alignment][:_format-Cutf8]}$(BR)$(BR)
                  $(UL $(LI index $(BR)
                    An integer indicating the element in a list to _format.)
                  $(LI alignment $(BR)
                    An optional integer indicating the minimum width. The
                    result is padded with spaces if the length of the value
                    is less than alignment.)
                  $(LI _format-Cutf8 $(BR)
                    An optional Cutf8 of formatting codes.)
                )$(BR)

                The leading and trailing braces are required. To include a
                literal brace character, use two leading or trailing brace
                characters.$(BR)$(BR)
                If formatStr is "{0} bottles of beer on the wall" and the
                argument is an int with the value of 99, the return value
                will be:$(BR) "99 bottles of beer on the wall".

        **********************************************************************/

        public final invariant(T)[] convert (const(T)[] formatStr, ...)
        {
                return convert (_arguments, _argptr, formatStr);
        }

        /**********************************************************************

        **********************************************************************/

        public final uint convert (Sink sink, const(T)[] formatStr, ...)
        {
                return convert (sink, _arguments, _argptr, formatStr);
        }

        /**********************************************************************

        **********************************************************************/

        public final invariant(T)[] convert (TypeInfo[] arguments, ArgList args, const(T)[] formatStr)
        {
                T[] output;

                uint sink (const(T)[] s)
                {
                        output ~= s;
                        return s.length;
                }

                convert (&sink, arguments, args, formatStr);
                return output.idup;
        }

        /**********************************************************************

        **********************************************************************/

        public final const(T)[] convertOne (T[] result, TypeInfo ti, Arg arg)
        {
                return munge (result, null, ti, arg);
        }

        /**********************************************************************

        **********************************************************************/

        public final uint convert (Sink sink, TypeInfo[] arguments, ArgList args, const(T)[] formatStr)
        {
                assert (formatStr, "null format specifier");
                assert (arguments.length < 64, "too many args in Layout.convert");

                version (GNU)
                        {
                        Arg[64] arglist = void;
                        int[64] intargs = void;
                        byte[64] byteargs = void;
                        long[64] longargs = void;
                        short[64] shortargs = void;
                        void[][64] voidargs = void;
                        real[64] realargs = void;
                        float[64] floatargs = void;
                        double[64] doubleargs = void;

                        foreach (i, arg; arguments)
                                {
                                static if (is(typeof(args.ptr)))
                                    arglist[i] = args.ptr;
                                else
                                    arglist[i] = args;
                                /* Since floating point types don't live on
                                 * the stack, they must be accessed by the
                                 * correct type. */
                                bool converted = false;
                                switch (arg.classinfo.name[9])
                                       {
                                       case TypeCode.FLOAT:
                                            floatargs[i] = va_arg!(float)(args);
                                            arglist[i] = &floatargs[i];
                                            converted = true;
                                            break;

                                       case TypeCode.DOUBLE:
                                            doubleargs[i] = va_arg!(double)(args);
                                            arglist[i] = &doubleargs[i];
                                            converted = true;
                                            break;

                                       case TypeCode.REAL:
                                            realargs[i] = va_arg!(real)(args);
                                            arglist[i] = &realargs[i];
                                            converted = true;
                                            break;

                                       default:
                                            break;
                                        }
                                if (! converted)
                                   {
                                   switch (arg.tsize)
                                          {
                                          case 1:
                                               byteargs[i] = va_arg!(byte)(args);
                                               arglist[i] = &byteargs[i];
                                               break;
                                          case 2:
                                               shortargs[i] = va_arg!(short)(args);
                                               arglist[i] = &shortargs[i];
                                               break;
                                          case 4:
                                               intargs[i] = va_arg!(int)(args);
                                               arglist[i] = &intargs[i];
                                               break;
                                          case 8:
                                               longargs[i] = va_arg!(long)(args);
                                               arglist[i] = &longargs[i];
                                               break;
                                          case 16:
                                               voidargs[i] = va_arg!(void[])(args);
                                               arglist[i] = &voidargs[i];
                                               break;
                                          default:
                                               assert (false, "Unknown size: " ~ Integer.toString (arg.tsize) ~ " for " ~ arg.classinfo.name);
                                          }
                                   }
                                }
                        }
                     else
                        {
                        Arg[64] arglist = void;
                        foreach (i, arg; arguments)
                                {
                                arglist[i] = args;
                                args += (arg.tsize + int.sizeof - 1) & ~ (int.sizeof - 1);
                                }
                        }
                return parse (formatStr, arguments, arglist, sink);
        }

        /**********************************************************************

                Parse the format-Cutf8, emitting formatted args and text
                fragments as we go.

        **********************************************************************/

        private uint parse (const(T)[] layout, TypeInfo[] ti, Arg[] args, Sink sink)
        {
                T[384] result = void;
                int length, nextIndex;


                const(T)* s = layout.ptr;
                const(T)* fragment = s;
                const(T)* end = s + layout.length;

                while (true)
                      {
                      while (s < end && *s != '{')
                             ++s;

                      // emit fragment
                      length += sink (fragment [0 .. s - fragment]);

                      // all done?
                      if (s is end)
                          break;

                      // check for "{{" and skip if so
                      if (*++s is '{')
                         {
                         fragment = s++;
                         continue;
                         }

                      int index = 0;
                      bool indexed = false;

                      // extract index
                      while (*s >= '0' && *s <= '9')
                            {
                            index = index * 10 + *s++ -'0';
                            indexed = true;
                            }

                      // skip spaces
                      while (s < end && *s is ' ')
                             ++s;

                      bool crop;
                      bool left;
                      bool right;
                      int  width;

                      // has minimum or maximum width?
                      if (*s is ',' || *s is '.')
                         {
                         if (*s is '.')
                             crop = true;

                         while (++s < end && *s is ' ') {}
                         if (*s is '-')
                            {
                            left = true;
                            ++s;
                            }
                         else
                            right = true;

                         // get width
                         while (*s >= '0' && *s <= '9')
                                width = width * 10 + *s++ -'0';

                         // skip spaces
                         while (s < end && *s is ' ')
                                ++s;
                         }

                      const(T)[] format;

                      // has a format Cutf8?
                      if (*s is ':' && s < end)
                         {
                         const(T)* fs = ++s;

                         // eat everything up to closing brace
                         while (s < end && *s != '}')
                                ++s;
                         format = fs [0 .. s - fs];
                         }

                      // insist on a closing brace
                      if (*s != '}')
                         {
                         length += sink ("{malformed format}");
                         continue;
                         }

                      // check for default index & set next default counter
                      if (! indexed)
                            index = nextIndex;
                      nextIndex = index + 1;

                      // next char is start of following fragment
                      fragment = ++s;

                      // handle alignment
                      void process (const(T)[] str)
                      {
                                int padding = width - str.length;

                                if (crop)
                                   {
                                   if (padding < 0)
                                      {
                                      if (left)
                                         {
                                         length += sink ("...");
                                         length += sink (Utf.cropLeft (str[-padding..$]));
                                         }
                                      else
                                         {
                                         length += sink (Utf.cropRight (str[0..width]));
                                         length += sink ("...");
                                         }
                                      }
                                   else
                                       length += sink (str);
                                   }
                                else
                                   {
                                   // if right aligned, pad out with spaces
                                   if (right && padding > 0)
                                       length += spaces (sink, padding);

                                   // emit formatted argument
                                   length += sink (str);

                                   // finally, pad out on right
                                   if (left && padding > 0)
                                       length += spaces (sink, padding);
                                   }
                      }

                      // an astonishing number of typehacks needed to handle arrays :(
                      void processElement (TypeInfo _ti, Arg _arg)
                      {
                                if (_ti.classinfo.name.length is 20 && _ti.classinfo.name[9..$] == "StaticArray" )
                                   {
                                   auto tiStat = cast(TypeInfo_StaticArray)_ti;
                                   auto p = _arg;
                                   length += sink ("[");
                                   for (int i = 0; i < tiStat.len; i++)
                                       {
                                       if (p !is _arg )
                                           length += sink (", ");
                                       processElement (tiStat.value, p);
                                       p += tiStat.tsize/tiStat.len;
                                       }
                                   length += sink ("]");
                                   }
                                else
                                if (_ti.classinfo.name.length is 25 && _ti.classinfo.name[9..$] == "AssociativeArray")
                                   {
                                   auto tiAsso = cast(TypeInfo_AssociativeArray)_ti;
                                   auto tiKey = tiAsso.key;
                                   auto tiVal = tiAsso.next();
                                   // the knowledge of the internal k/v storage is used
                                   // so this might break if, that internal storage changes
                                   alias ubyte AV; // any type for key, value might be ok, the sizes are corrected later
                                   alias ubyte AK;
                                   auto aa = *cast(AV[AK]*) _arg;

                                   length += sink ("{");
                                   bool first = true;

                                   int roundUp (int sz)
                                   {
                                        return (sz + (void*).sizeof -1) & ~((void*).sizeof - 1);
                                   }

                                   foreach (inout v; aa)
                                           {
                                           // the key is befor the value, so substrace with fixed key size from above
                                           auto pk = cast(Arg)( &v - roundUp(AK.sizeof));
                                           // now the real value pos is plus the real key size
                                           auto pv = cast(Arg)(pk + roundUp(tiKey.tsize()));

                                           if (!first)
                                                length += sink (", ");
                                           processElement (tiKey, pk);
                                           length += sink (" => ");
                                           processElement (tiVal, pv);
                                           first = false;
                                           }
                                   length += sink ("}");
                                   }
                                else
                                if (_ti.classinfo.name[9] is TypeCode.ARRAY &&
                                   (_ti !is typeid(invariant(char)[]))  &&
                                   (_ti !is typeid(invariant(wchar)[])) &&
                                   (_ti !is typeid(invariant(dchar)[])) &&

                                   (_ti !is typeid(const(char)[]))  &&
                                   (_ti !is typeid(const(wchar)[])) &&
                                   (_ti !is typeid(const(dchar)[])) &&

                                   (_ti !is typeid(char[]))  &&
                                   (_ti !is typeid(wchar[])) &&
                                   (_ti !is typeid(dchar[])))
                                   {
                                   // for all non string array types (including char[][])
                                   auto arr = *cast(void[]*)_arg;
                                   auto len = arr.length;
                                   auto ptr = cast(Arg) arr.ptr;
                                   auto elTi = _ti.next();
                                   auto size = elTi.tsize();
                                   length += sink ("[");
                                   while (len > 0)
                                         {
                                         if (ptr !is arr.ptr)
                                             length += sink (", ");
                                         processElement (elTi, ptr);
                                         len -= 1;
                                         ptr += size;
                                         }
                                   length += sink ("]");
                                   }
                                else
                                   // the standard processing
                                   process (munge(result, format, _ti, _arg));
                      }


                      // process this argument
                      if (index >= ti.length)
                          process ("{invalid index}");
                      else
                         processElement (ti[index], args[index]);
                      }
                return length;
        }

        /**********************************************************************

        **********************************************************************/

        private void error (invariant(T)[] msg)
        {
                throw new IllegalArgumentException (msg);
        }

        /**********************************************************************

        **********************************************************************/

        private uint spaces (Sink sink, int count)
        {
                uint ret;

                static T[32] Spaces = ' ';
                while (count > Spaces.length)
                      {
                      ret += sink (Spaces);
                      count -= Spaces.length;
                      }
                return ret + sink (Spaces[0..count]);
        }

        /***********************************************************************

        ***********************************************************************/

        private const(T)[] munge (T[] result, const(T)[] format, TypeInfo type, Arg p)
        {
                // remove leading const/invariant.  Note that we only ever
                // refer to const versions of the type or call const methods,
                // so this is ok.
                //
                if(auto ti = cast(TypeInfo_Const)type)
                  type = ti.next;

                switch (type.classinfo.name[9])
                       {
                       case TypeCode.ARRAY:

                            //
                            // this should handle all forms of d/w/char[]
                            // arrays including invariant and const.
                            //
                            TypeInfo elemType = type.next;
                            if(auto ti = cast(TypeInfo_Const)elemType)
                              elemType = ti.next;

                            if (elemType is typeid(char))
                                return Utf.fromString8 (*cast(Cutf8*) p, result);

                            if (elemType is typeid(wchar))
                                return Utf.fromString16 (*cast(Cutf16*) p, result);

                            if (elemType is typeid(dchar))
                                return Utf.fromString32 (*cast(Cutf32*) p, result);

                            return Utf.fromString8 (type.toString, result);

                       case TypeCode.BOOL:
                            static const(T)[] t = "true";
                            static const(T)[] f = "false";
                            return (*cast(bool*) p) ? t : f;

                       case TypeCode.BYTE:
                            return integer (result, *cast(const byte*) p, format, ubyte.max);

                       case TypeCode.VOID:
                       case TypeCode.UBYTE:
                            return integer (result, *cast(const ubyte*) p, format, ubyte.max, "u");

                       case TypeCode.SHORT:
                            return integer (result, *cast(const short*) p, format, ushort.max);

                       case TypeCode.USHORT:
                            return integer (result, *cast(const ushort*) p, format, ushort.max, "u");

                       case TypeCode.INT:
                            return integer (result, *cast(const int*) p, format, uint.max);

                       case TypeCode.UINT:
                            return integer (result, *cast(const uint*) p, format, uint.max, "u");

                       case TypeCode.ULONG:
                            return integer (result, *cast(const long*) p, format, ulong.max, "u");

                       case TypeCode.LONG:
                            return integer (result, *cast(const long*) p, format, ulong.max);

                       case TypeCode.FLOAT:
                            return floater (result, *cast(const float*) p, format);

                       case TypeCode.DOUBLE:
                            return floater (result, *cast(const double*) p, format);

                       case TypeCode.REAL:
                            return floater (result, *cast(const real*) p, format);

                       case TypeCode.CHAR:
                            return Utf.fromString8 ((cast(const char*) p)[0..1], result);

                       case TypeCode.WCHAR:
                            return Utf.fromString16 ((cast(const wchar*) p)[0..1], result);

                       case TypeCode.DCHAR:
                            return Utf.fromString32 ((cast(const dchar*) p)[0..1], result);

                       case TypeCode.POINTER:
                            return integer (result, *cast(const size_t*) p, format, size_t.max, "x");

                       case TypeCode.CLASS:
                            auto c = *cast(Object*) p;
                            if (c)
                                return Utf.fromString8 (c.toString, result);
                            break;

                       case TypeCode.STRUCT:
                            auto s = cast(TypeInfo_Struct) type;
                            if (s.xtoString)
                                return Utf.fromString8 (s.xtoString(p), result);
                            goto default;

                       case TypeCode.INTERFACE:
                            // TODO: toUtf8 is not a const function, so we
                            // cannot cast to const Object.  However, I'm not
                            // sure how to declare const member functions, so
                            // we'll wait until that debate is settled.  For
                            // now, we just cast away const.  This goes for
                            // interface and class.  This should be relatively
                            // safe because toUtf8 should be const for the
                            // most part.
                            auto x = *cast(void**) p;
                            if (x)
                               {
                               auto pi = **cast(Interface ***) x;
                               auto o = cast(Object)(*cast(void**)p - pi.offset);
                               return Utf.fromString8 (o.toString, result);
                               }
                            break;

                       case TypeCode.ENUM:
                            return munge (result, format, (cast(TypeInfo_Enum) type).base, p);

                       case TypeCode.TYPEDEF:
                            return munge (result, format, (cast(TypeInfo_Typedef) type).base, p);

                       default:
                            return unknown (result, format, type, p);
                       }

                return cast(const(T)[]) "{null}";
        }

        /**********************************************************************

        **********************************************************************/

        protected const(T)[] unknown (T[] result, const(T)[] format, TypeInfo type, Arg p)
        {
                return cast(T[])"{unhandled argument type: " ~ Utf.fromString8 (cast(T[])type.toString, result) ~ cast(T[])"}";
        }

        /**********************************************************************

        **********************************************************************/

        protected const(T)[] integer (T[] output, long v, const(T)[] format, ulong mask = ulong.max, const(T)[] def="d")
        {
                if (format.length is 0)
                    format = def;
                if (format[0] != 'd')
                    v &= mask;
                return Integer.format!(T) (output, v, format);
        }

        /**********************************************************************

        **********************************************************************/

        protected const(T)[] floater (T[] output, real v, const(T)[] format)
        {
                T    style = 'f';
                uint places = 2;

                parseGeneric (format, places, style);
                return Float.format (output, v, places, (style is 'e' || style is 'E') ? 0 : 10);
        }

        /**********************************************************************

        **********************************************************************/

        private bool parseGeneric (const(T)[] format, ref uint width, ref T style)
        {
                if (format.length)
                   {
                   uint number;
                   auto p = format.ptr;
                   auto e = p + format.length;
                   style = *p;
                   while (++p < e)
                          if (*p >= '0' && *p <= '9')
                              number = number * 10 + *p - '0';
                          else
                             break;

                   if (p - format.ptr > 1)
                      {
                      width = number;
                      return true;
                      }
                   }
                return false;
        }
}


/*******************************************************************************

*******************************************************************************/

private enum TypeCode
{
        EMPTY = 0,
        VOID = 'v',
        BOOL = 'b',
        UBYTE = 'h',
        BYTE = 'g',
        USHORT = 't',
        SHORT = 's',
        UINT = 'k',
        INT = 'i',
        ULONG = 'm',
        LONG = 'l',
        REAL = 'e',
        FLOAT = 'f',
        DOUBLE = 'd',
        CHAR = 'a',
        WCHAR = 'u',
        DCHAR = 'w',
        ARRAY = 'A',
        CLASS = 'C',
        STRUCT = 'S',
        ENUM = 'E',
        CONST = 'x',
        INVARIANT = 'y',
        DELEGATE = 'D',
        FUNCTION = 'F',
        POINTER = 'P',
        TYPEDEF = 'T',
        INTERFACE = 'I',
}



/*******************************************************************************

*******************************************************************************/

debug (UnitTest)
{
        unittest
        {
        alias Layout!(char).convert Formatter;

        assert( Formatter( "abc" ) == "abc" );
        assert( Formatter( "{0}", 1 ) == "1" );
        assert( Formatter( "{0}", -1 ) == "-1" );

        assert( Formatter( "{}", 1 ) == "1" );
        assert( Formatter( "{} {}", 1, 2) == "1 2" );
        assert( Formatter( "{} {0} {}", 1, 3) == "1 1 3" );
        assert( Formatter( "{} {0} {} {}", 1, 3) == "1 1 3 {invalid index}" );
        assert( Formatter( "{} {0} {} {:x}", 1, 3) == "1 1 3 {invalid index}" );

        assert( Formatter( "{0}", true ) == "true" , Formatter( "{0}", true ));
        assert( Formatter( "{0}", false ) == "false" );

        assert( Formatter( "{0}", cast(byte)-128 ) == "-128" );
        assert( Formatter( "{0}", cast(byte)127 ) == "127" );
        assert( Formatter( "{0}", cast(ubyte)255 ) == "255" );

        assert( Formatter( "{0}", cast(short)-32768  ) == "-32768" );
        assert( Formatter( "{0}", cast(short)32767 ) == "32767" );
        assert( Formatter( "{0}", cast(ushort)65535 ) == "65535" );
        // assert( Formatter( "{0:x4}", cast(ushort)0xafe ) == "0afe" );
        // assert( Formatter( "{0:X4}", cast(ushort)0xafe ) == "0AFE" );

        assert( Formatter( "{0}", -2147483648 ) == "-2147483648" );
        assert( Formatter( "{0}", 2147483647 ) == "2147483647" );
        assert( Formatter( "{0}", 4294967295 ) == "4294967295" );
        // compiler error
        assert( Formatter( "{0}", -9223372036854775807L) == "-9223372036854775807" );
        assert( Formatter( "{0}", 0x8000_0000_0000_0000L) == "9223372036854775808" );
        assert( Formatter( "{0}", 9223372036854775807L ) == "9223372036854775807" );
        // Error: prints -1
        // assert( Formatter( "{0}", 18446744073709551615UL ) == "18446744073709551615" );

        assert( Formatter( "{0}", "s" ) == "s" );
        // fragments before and after
        assert( Formatter( "d{0}d", "s" ) == "dsd" );
        assert( Formatter( "d{0}d", "1234567890" ) == "d1234567890d" );

        // brace escaping
        assert( Formatter( "d{0}d", "<Cutf8>" ) == "d<Cutf8>d");
        assert( Formatter( "d{{0}d", "<Cutf8>" ) == "d{0}d");
        assert( Formatter( "d{{{0}d", "<Cutf8>" ) == "d{<Cutf8>d");
        assert( Formatter( "d{0}}d", "<Cutf8>" ) == "d<Cutf8>}d");

        assert( Formatter( "{0:x}", 0xafe0000 ) == "afe0000" );
        // todo: is it correct to print 7 instead of 6 chars???
        assert( Formatter( "{0:x7}", 0xafe0000 ) == "afe0000" );
        assert( Formatter( "{0:x8}", 0xafe0000 ) == "0afe0000" );
        assert( Formatter( "{0:X8}", 0xafe0000 ) == "0AFE0000" );
        assert( Formatter( "{0:X9}", 0xafe0000 ) == "00AFE0000" );
        assert( Formatter( "{0:X13}", 0xafe0000 ) == "000000AFE0000" );
        assert( Formatter( "{0:x13}", 0xafe0000 ) == "000000afe0000" );
        // decimal width
        assert( Formatter( "{0:d6}", 123 ) == "000123" );
        assert( Formatter( "{0,7:d6}", 123 ) == " 000123" );
        assert( Formatter( "{0,-7:d6}", 123 ) == "000123 " );

        assert( Formatter( "{0:d7}", -123 ) == "-0000123" );
        assert( Formatter( "{0,7:d6}", 123 ) == " 000123" );
        assert( Formatter( "{0,7:d7}", -123 ) == "-0000123" );
        assert( Formatter( "{0,8:d7}", -123 ) == "-0000123" );
        assert( Formatter( "{0,5:d7}", -123 ) == "-0000123" );

        assert( Formatter( "{0:X}", 0xFFFF_FFFF_FFFF_FFFF) == "FFFFFFFFFFFFFFFF" );
        assert( Formatter( "{0:x}", 0xFFFF_FFFF_FFFF_FFFF) == "ffffffffffffffff" );
        assert( Formatter( "{0:x}", 0xFFFF_1234_FFFF_FFFF) == "ffff1234ffffffff" );
        assert( Formatter( "{0:x19}", 0x1234_FFFF_FFFF) == "00000001234ffffffff" );
        // Error: prints -1
        // assert( Formatter( "{0}", 18446744073709551615UL ) == "18446744073709551615" );
        assert( Formatter( "{0}", "s" ) == "s" );
        // fragments before and after
        assert( Formatter( "d{0}d", "s" ) == "dsd" );

        // Negative numbers in various bases
        assert( Formatter( "{:b}", cast(byte) -1 ) == "11111111" );
        assert( Formatter( "{:b}", cast(short) -1 ) == "1111111111111111" );
        assert( Formatter( "{:b}", cast(int) -1 )
                == "11111111111111111111111111111111" );
        assert( Formatter( "{:b}", cast(long) -1 )
                == "1111111111111111111111111111111111111111111111111111111111111111" );

        assert( Formatter( "{:o}", cast(byte) -1 ) == "377" );
        assert( Formatter( "{:o}", cast(short) -1 ) == "177777" );
        assert( Formatter( "{:o}", cast(int) -1 ) == "37777777777" );
        assert( Formatter( "{:o}", cast(long) -1 ) == "1777777777777777777777" );

        assert( Formatter( "{:d}", cast(byte) -1 ) == "-1" );
        assert( Formatter( "{:d}", cast(short) -1 ) == "-1" );
        assert( Formatter( "{:d}", cast(int) -1 ) == "-1" );
        assert( Formatter( "{:d}", cast(long) -1 ) == "-1" );

        assert( Formatter( "{:x}", cast(byte) -1 ) == "ff" );
        assert( Formatter( "{:x}", cast(short) -1 ) == "ffff" );
        assert( Formatter( "{:x}", cast(int) -1 ) == "ffffffff" );
        assert( Formatter( "{:x}", cast(long) -1 ) == "ffffffffffffffff" );

        // argument index
        assert( Formatter( "a{0}b{1}c{2}", "x", "y", "z" ) == "axbycz" );
        assert( Formatter( "a{2}b{1}c{0}", "x", "y", "z" ) == "azbycx" );
        assert( Formatter( "a{1}b{1}c{1}", "x", "y", "z" ) == "aybycy" );

        // alignment
        // align does not restrict the length
        assert( Formatter( "{0,5}", "hellohello" ) == "hellohello" );
        // align fills with spaces
        assert( Formatter( "->{0,-10}<-", "hello" ) == "->hello     <-" );
        assert( Formatter( "->{0,10}<-", "hello" ) == "->     hello<-" );
        assert( Formatter( "->{0,-10}<-", 12345 ) == "->12345     <-" );
        assert( Formatter( "->{0,10}<-", 12345 ) == "->     12345<-" );

        assert( Formatter( "{0:f}", 1.23f ) == "1.23" );
        assert( Formatter( "{0:f4}", 1.23456789L ) == "1.2346" );
        assert( Formatter( "{0:e4}", 0.0001) == "0.1000e-03");

        int[] a = [ 51, 52, 53, 54, 55 ];
        assert( Formatter( "{}", a ) == "[51, 52, 53, 54, 55]" );
        assert( Formatter( "{:x}", a ) == "[33, 34, 35, 36, 37]" );
        assert( Formatter( "{,-4}", a ) == "[51  , 52  , 53  , 54  , 55  ]" );
        assert( Formatter( "{,4}", a ) == "[  51,   52,   53,   54,   55]" );
        int[][] b = [ [ 51, 52 ], [ 53, 54, 55 ] ];
        assert( Formatter( "{}", b ) == "[[51, 52], [53, 54, 55]]" );

        ushort[3] c = [ cast(ushort)51, 52, 53 ];
        assert( Formatter( "{}", c ) == "[51, 52, 53]" );

        ushort[long] d;
        d[234] = 2;
        d[345] = 3;
        assert( Formatter( "{}", d ) == "{234 => 2, 345 => 3}" );

        bool[char[]] e;
        e[ "key".dup ] = true;
        e[ "value".dup ] = false;
        assert( Formatter( "{}", e ) == "{key => true, value => false}" );

        char[][ double ] f;
        f[ 1.0 ] = "one".dup;
        f[ 3.14 ] = "PI".dup;
        assert( Formatter( "{}", f ) == "{1.00 => one, 3.14 => PI}" );
        }
}
