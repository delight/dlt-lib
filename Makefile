all: libdlt.so

VPATH := ${SRCDIR}

PHOBOS_SRC_DIR = ${DELIGHT_GCC}/include/d2/4.1.2

vpath %.d ${PHOBOS_SRC_DIR}

TI=ti_AC.o ti_Ag.o ti_Aint.o ti_Along.o ti_Ashort.o \
	ti_C.o \
	ti_byte.o ti_cdouble.o ti_cfloat.o ti_char.o ti_creal.o \
	ti_dchar.o ti_delegate.o ti_double.o ti_float.o ti_idouble.o ti_ifloat.o \
	ti_int.o  ti_ireal.o ti_long.o ti_ptr.o ti_real.o ti_short.o ti_ubyte.o \
	ti_uint.o ti_ulong.o ti_ushort.o ti_wchar.o \
	ti_Afloat.o ti_Adouble.o ti_Areal.o \
	ti_Acfloat.o ti_Acdouble.o ti_Acreal.o \
	ti_void.o

MAIN_OBJS=std/asserterror.o std/hiddenfunc.o gcstats.o \
	std/outofmemory.o std/compiler.o std/system.o std/moduleinit.o std/md5.o std/base64.o \
	std/string.o std/math.o \
	std/outbuffer.o std/ctype.o std/regexp.o std/random.o \
	std/stream.o std/cstream.o std/switcherr.o std/array.o std/gc.o \
	std/thread.o std/utf.o std/uri.o \
	crc32.o std/conv.o \
	std/syserror.o std/metastrings.o \
	std/c/stdarg.o std/c/stdio.o std/stdio.o std/format.o \
	std/openrj.o std/uni.o std/demangle.o std/bitarray.o \
	$(subst ti_,std/typeinfo/ti_,$(TI)) \
	std/date.o std/dateparse.o etc/c/zlib.o std/zlib.o std/zip.o \
	std/stdarg.o \
	std/signals.o std/cpuid.o std/traits.o std/typetuple.o std/bind.o \
	std/algorithm.o std/bitmanip.o std/contracts.o std/functional.o \
	std/getopt.o std/numeric.o std/slist.o std/typecons.o std/variant.o \
	std/cover.o std/complex.o std/encoding.o std/iterator.o std/xml.o \
	std/bigint.o

#PHOBOS_SOURCES = $(wildcard ${PHOBOS_SRC_DIR}/std/**/*.d)
DLT_SOURCES = $(wildcard ${SRCDIR}/dlt/*.dlt) $(wildcard ${SRCDIR}/dlt/*/*.dlt) $(patsubst ${SRCDIR}/_externals/dlt/gtk.dlt,,$(wildcard ${SRCDIR}/_externals/*/*.dlt))
D_SOURCES = $(wildcard ${SRCDIR}/dlt/*/*/*.d)
SOURCES = ${DLT_SOURCES} ${D_SOURCES}

PHOBOS_OBJECTS = ${MAIN_OBJS}
OBJECTS = $(patsubst ${SRCDIR}/%.dlt,%.o,${DLT_SOURCES}) $(patsubst ${SRCDIR}/%.d,%.o,${D_SOURCES})

#VERSION = -fversion=GNU_pthread_suspend -fversion=Linux -fversion=Unix -fversion=GNU_Need_execvpe -fversion=Posix -funittest
#-fd-verbose --save-temps
#VERSION=-funittest
VERSION=-fPIC

%.o: %.dlt
	[ -d `dirname $@` ] || mkdir -p `dirname $@`
	${DELIGHT_GCC}/bin/gdc ${VERSION} -c -g -o "$@" -I${SRCDIR} "$<"

%.o: %.d
	[ -d `dirname $@` ] || mkdir -p `dirname $@`
	${DELIGHT_GCC}/bin/gdc ${VERSION} -c -g -o "$@" -I${SRCDIR} "$<"

#%.o: ${PHOBOS_SRC_DIR}/%.d
#	[ -d `dirname $@` ] || mkdir -p `dirname $@`
#	${DELIGHT_GCC}/bin/gdc ${VERSION} -c -g -o "$@" -I${PHOBOS_SRC_DIR} "$<"

libdlt.so: ${OBJECTS} ${PHOBOS_OBJECTS}
	${DELIGHT_GCC}/bin/gdc -shared -Wl,-soname,libdlt.so -o $@ $^

libdltgtk.so: _externals/dlt/gtk.o
	${DELIGHT_GCC}/bin/gdc --as-needed `pkg-config --libs gtk+-2.0` -shared -Wl,-soname,libdltgtk.so -o $@ $^

install: libdlt.so libdltgtk.so
	[ -d "${DISTDIR}/lib" ] || mkdir ${DISTDIR}/lib
	[ -d "${DISTDIR}/include" ] || mkdir ${DISTDIR}/include
	cp libdlt.so ${DISTDIR}/lib
	cp libdltgtk.so ${DISTDIR}/lib
	if [ -d "${DELIGHT_GCC}/lib64" ]; then \
	  cp ${DELIGHT_GCC}/lib64/libgphobos2.so ${DISTDIR}/lib; \
	else \
	  cp ${DELIGHT_GCC}/lib/libgphobos2.so ${DISTDIR}/lib; \
	fi
	cp -r "${SRCDIR}/dlt" "${DISTDIR}/include/"
	cp -r "${SRCDIR}/_externals" "${DISTDIR}/include/"

clean:
	rm -f ${OBJECTS}
